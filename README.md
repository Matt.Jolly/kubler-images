## kangie

This is a collection of prototype image definitions for things that I've found it useful
to stuff into a box.

These are eerily similar to images that are running (or have run) in production _somewhere_.

Maintainer: Matt Jolly <Matt.Jolly@footclan.ninja>
