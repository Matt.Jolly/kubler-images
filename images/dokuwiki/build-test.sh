#!/usr/bin/env sh

set -e

# does the webserver output something sane?
/usr/sbin/lighttpd -v | grep -q lighttpd || exit 1
# the built-in config validator should pass the shipped configuration
/usr/sbin/lighttpd-angel -t -f /etc/lighttpd/lighttpd.conf || exit 1

exit 0
