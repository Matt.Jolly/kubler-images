### kangie/dokuwiki:20230704

Built: Fri Jul 21 14:55:32 AEST 2023
Image Size: 117MB

#### Installed
Package | USE Flags
--------|----------
acct-group/lighttpd-0-r2 | ``
acct-group/nullmail-0-r1 | ``
acct-user/lighttpd-0-r2 | ``
acct-user/nullmail-0-r1 | ``
app-admin/eselect-1.4.25 | `-doc -emacs -vim-syntax`
app-alternatives/bzip2-1 | `reference (split-usr) -lbzip2 -pbzip2`
app-arch/bzip2-1.0.8-r4 | `(split-usr) -static -static-libs -verify-sig`
app-eselect/eselect-php-0.9.8 | `fpm -apache2`
app-misc/ca-certificates-20230311.3.90 | `-cacert`
dev-lang/php-8.1.16 | `acl bzip2 cli ctype fileinfo filter flatfile fpm gd iconv nls opcache phar posix readline session simplexml ssl threads tokenizer unicode xml zlib -apache2 -apparmor -argon2 -bcmath -berkdb -calendar -cdb -cgi -cjk (-coverage) -curl -debug -embed -enchant -exif -ffi (-firebird) -ftp -gdbm -gmp -imap -inifile -intl -iodbc -ipv6 -jit -kerberos -ldap -ldap-sasl -libedit -lmdb -mhash -mssql -mysql -mysqli (-oci8-instant-client) -odbc -pcntl -pdo -phpdbg -postgres -qdbm (-selinux) -session-mm -sharedmem -snmp -soap -sockets -sodium -spell -sqlite (-systemd) -sysvipc -test -tidy -tokyocabinet -truetype -webp -xmlreader -xmlwriter -xpm -xslt -zip`
dev-libs/expat-2.5.0 | `unicode -examples -static-libs -test`
dev-libs/gmp-6.2.1-r5 | `asm cpudetection cxx -doc -pic -static-libs`
dev-libs/libpcre2-10.42-r1 | `bzip2 jit pcre16 readline (split-usr) unicode zlib -libedit -pcre32 -static-libs -valgrind -verify-sig`
dev-libs/libtasn1-4.19.0 | `-static-libs -test -verify-sig`
dev-libs/libunistring-1.1-r1 | `-doc -static-libs`
dev-libs/libxml2-2.11.4 | `ftp readline -debug -examples -icu -lzma -python -static-libs -test`
dev-libs/nettle-3.9.1 | `asm gmp -doc -static-libs -verify-sig`
dev-libs/oniguruma-6.9.8 | `-crnl-as-line-terminator -static-libs`
dev-libs/openssl-3.0.9-r1 | `asm -fips -ktls -rfc3779 -sctp -static-libs -test -tls-compression -vanilla -verify-sig -weak-ssl-ciphers`
mail-mta/nullmailer-2.2-r2 | `ssl -test`
media-libs/libjpeg-turbo-2.1.5.1 | `-java -static-libs`
media-libs/libpng-1.6.39 | `-apng -static-libs`
net-dns/libidn2-2.3.4 | `nls -static-libs -verify-sig`
net-libs/gnutls-3.7.8 | `cxx idn nls openssl seccomp tls-heartbeat zlib -brotli -dane -doc -examples -guile -pkcs11 (-sslv2) (-sslv3) -static-libs -test (-test-full) -tools -verify-sig -zstd`
net-misc/curl-8.2.0 | `openssl ssl -adns -alt-svc -brotli -ftp -gnutls -gopher -hsts -http2 -idn -imap -kerberos -ldap -mbedtls (-nghttp3) -nss -pop3 -progress-meter -rtmp -rustls -samba -smtp -ssh (-sslv3) -static-libs -telnet -test -tftp -verify-sig -websockets -zstd`
sys-apps/acl-2.3.1-r1 | `nls (split-usr) -static-libs`
sys-apps/attr-2.5.1-r2 | `nls (split-usr) -debug -static-libs`
sys-apps/coreutils-9.3-r3 | `acl nls openssl (split-usr) (xattr) -caps -gmp -hostname -kill -multicall (-selinux) -static -test -vanilla -verify-sig`
sys-apps/debianutils-5.7 | `-installkernel -static`
sys-apps/file-5.44-r3 | `bzip2 seccomp zlib -lzip -lzma -python -static-libs -verify-sig -zstd`
sys-apps/shadow-4.13-r4 | `acl nls (split-usr) xattr -audit -bcrypt -cracklib -pam (-selinux) -skey -su -verify-sig`
sys-apps/systemd-utils-253.6 | `(split-usr) tmpfiles -acl -boot -kmod (-selinux) -sysusers -test -udev`
sys-apps/util-linux-2.38.1-r2 | `cramfs hardlink logger readline (split-usr) suid (unicode) -audit -build -caps -cryptsetup -fdformat -kill -magic -ncurses (-nls) -pam -python (-rtas) (-selinux) -slang -static-libs -su (-systemd) -test -tty-helpers -udev -verify-sig`
sys-devel/gettext-0.21.1 | `acl cxx openmp -doc -emacs -git -java -ncurses (-nls) -static-libs -verify-sig`
sys-kernel/linux-headers-6.1 | `-headers-only`
sys-libs/libcap-2.69 | `(split-usr) -pam -static-libs -tools`
sys-libs/libseccomp-2.5.4 | `(-experimental-loong) -python -static-libs -test`
sys-libs/musl-1.2.3-r7 | `-crypt -headers-only -verify-sig`
sys-libs/ncurses-6.4_p20230401 | `cxx (split-usr) (tinfo) -ada -debug -doc -gpm -minimal -profile (-stack-realign) -static-libs -test -trace -verify-sig`
sys-libs/readline-8.1_p2-r1 | `(split-usr) (unicode) -static-libs -utils -verify-sig`
sys-libs/zlib-1.2.13-r1 | `(split-usr) -minizip -static-libs -verify-sig`
www-apps/dokuwiki-20230404a | `gd -vhosts`
www-servers/lighttpd-1.4.71 | `-brotli -dbi -gnutls -kerberos -ldap -lua -maxminddb -mbedtls -mmap -mysql -nettle -nss -pcre -php -postgres -rrdtool -sasl (-selinux) -sqlite -ssl -system-xxhash -test -unwind -webdav -xattr -zlib -zstd`
#### Inherited
Package | USE Flags
--------|----------
**FROM kubler/busybox** |
sys-apps/busybox-1.34.1-r1 | `make-symlinks static -debug -ipv6 -livecd -math -mdev -pam -savedconfig (-selinux) -sep-usr -syslog (-systemd)`

#### Purged
- [x] Headers
- [x] Static Libs
