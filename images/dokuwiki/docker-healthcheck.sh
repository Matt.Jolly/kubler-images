#!/usr/bin/env sh

set -e

# Does the webserver serve even the most basic of content?
curl --fail http://localhost || exit 1

exit 0
