## kangie/dokuwiki

Run this [dokuwiki][] image with:

    $ docker run -d --name dokuwiki kangie/dokuwiki

[Last Build][packages]

[dokuwiki]: https://dokuwiki.url
[packages]: PACKAGES.md
