#
# Kubler phase 1 config, pick installed packages and/or customize the build
#

# The recommended way to install software is setting ${_packages}
# List of Gentoo package atoms to be installed at custom root fs ${_EMERGE_ROOT}, optional, space separated
# If you are not sure about package names you may want to start an interactive build container:
#     kubler.sh build -i kangie/dokuwiki
# ..and then search the Portage database:
#     eix <search-string>
_packages="
    www-servers/lighttpd
    www-apps/dokuwiki
    net-misc/curl
"
# Install a standard system directory layout at ${_EMERGE_ROOT}, optional, default: false
BOB_INSTALL_BASELAYOUT=true

# Remove specified binary cache file(s) *once* for given tag key..
#_no_cache_20200731="sys-apps/bash foo/bar/bar-0.1.2-r3.xpak"
# ..or omit the tag to always remove given binary cache file(s).
#_no_cache="sys-apps/bash"

# Define custom variables to your liking
#_dokuwiki_version=1.0

#
# This hook can be used to configure the build container itself, install packages, run any command, etc
#
configure_builder()
{
    # Packages installed in this hook don't end up in the final image but are available for depending image builds
    #emerge dev-lang/go app-misc/foo
    update_use 'app-admin/webapp-config' '-portage'
    emerge app-admin/webapp-config
    provide_package 'app-admin/webapp-config'
    :
}

#
# This hook is called just before starting the build of the root fs
#
configure_rootfs_build()
{
    # Let's grab the shiniest version of cURL and build a minimal configuration for health checks
    update_use 'net-misc/curl' '-use::*' '+ssl' '+openssl' '+curl_ssl_openssl'
    update_use 'www-apps/dokuwiki' '+gd'
    update_use 'dev-lang/php' '+use::gd' '+fpm'
    update_use 'app-eselect/eselect-php' '+fpm'
    update_use 'www-servers/lighttpd' '-use::*'

    update_use 'dev-libs/libxml2' '-python'
    # We need tmpfiles for lighttpd, but we don't need the rest of systemd-utils
    update_use 'sys-apps/systemd-utils' '-use::*' '+tmpfiles'
    # Let's build a lightweight imagemagick, we only need common formats for now
    update_use 'media-gfx/imagemagick' '-use::*' '+jpeg' '+png' '+svg' '+xml' '+webp'

    # One day I'll make an official container profile...
    update_use 'sys-apps/debianutils' '-installkernel'

    update_keywords 'media-gfx/imagemagick' '+~amd64'
    update_keywords 'net-misc/curl' '+~amd64'
    update_keywords 'www-apps/dokuwiki' '+~amd64'
    update_keywords 'www-servers/lighttpd' '+~amd64'

    # standard su issues for lightweight containers/chroots
    update_use 'sys-apps/util-linux' '-su'

    # webapp-config __sucks__ and we don't need it at runtime
    mkdir -p ${_EMERGE_ROOT}/etc/vhosts || die
    touch ${_EMERGE_ROOT}/etc/vhosts/webapp-config || die
    mkdir -p ${_EMERGE_ROOT}/sbin || die
    ln -s /sbin/webapp-config ${_EMERGE_ROOT}/sbin/webapp-config || die
    :
}

#
# This hook is called just before packaging the root fs tar ball, ideal for any post-install tasks, clean up, etc
#
finish_rootfs_build()
{
    # remove any remnants of the webapp-config hacks
    rm -rf "${_EMERGE_ROOT}"/etc/vhosts || die
    rm -f "${_EMERGE_ROOT}"/sbin/webapp-config || die
    # Configure lighttpd for php; according to the gentoo wiki the shipped mod_fastcgi.conf isn't
    # suitable for use with php-fpm.
    cat > "${_EMERGE_ROOT}"/etc/lighttpd/mod_fastcgi.conf << EOF || die unable to write mod_fastcgi.conf
server.modules += ("mod_fastcgi")
    fastcgi.server = ( ".php" =>
    ( "localhost" =>
        (
        "host" => "127.0.0.1",
        "port" => "9000"
        )
    )
)
EOF
    # enable mod_fastcgi.conf
    sed -i 's/#   include "mod_fastcgi.conf"/   include "mod_fastcgi.conf"/' \
        "${_EMERGE_ROOT}"/etc/lighttpd/lighttpd.conf || die unable to enable mod_fastcgi.conf
    :
}
