### kangie/openldap:20230704

Built: Mon Jul 17 16:44:32 AEST 2023
Image Size: 55.1MB

#### Installed
Package | USE Flags
--------|----------
acct-group/audio-0-r1 | ``
acct-group/cdrom-0-r1 | ``
acct-group/dialout-0-r1 | ``
acct-group/disk-0-r1 | ``
acct-group/floppy-0 | ``
acct-group/input-0-r1 | ``
acct-group/kmem-0-r1 | ``
acct-group/kvm-0-r1 | ``
acct-group/lp-0-r1 | ``
acct-group/render-0-r1 | ``
acct-group/sgx-0 | ``
acct-group/tape-0-r1 | ``
acct-group/tty-0-r1 | ``
acct-group/usb-0-r1 | ``
acct-group/video-0-r1 | ``
app-alternatives/bzip2-1 | `reference (split-usr) -lbzip2 -pbzip2`
app-arch/bzip2-1.0.8-r4 | `(split-usr) -static -static-libs -verify-sig`
app-arch/xz-utils-5.4.3 | `extra-filters nls (split-usr) -doc -static-libs -verify-sig`
app-arch/zstd-1.5.5 | `lzma (split-usr) zlib -lz4 -static-libs -test`
app-misc/ca-certificates-20230311.3.90 | `-cacert`
dev-db/lmdb-0.9.30 | `-static-libs`
dev-libs/libevent-2.1.12-r1 | `clock-gettime ssl -debug -malloc-replacement -static-libs -test -verbose-debug -verify-sig`
dev-libs/libltdl-2.4.7-r1 | `-static-libs`
dev-libs/libpcre2-10.42-r1 | `bzip2 jit pcre16 readline (split-usr) unicode zlib -libedit -pcre32 -static-libs -valgrind -verify-sig`
dev-libs/openssl-3.0.9-r1 | `asm -fips -ktls -rfc3779 -sctp -static-libs -test -tls-compression -vanilla -verify-sig -weak-ssl-ciphers`
net-nds/openldap-2.6.4-r2 | `cleartext crypt ssl syslog -argon2 -autoca -cxx -debug -experimental -gnutls -iodbc -kerberos -kinit -minimal -odbc -overlays -pbkdf2 -perl -samba -sasl (-selinux) -sha2 -smbkrb5passwd -static-libs (-systemd) -tcpd -test`
sys-apps/acl-2.3.1-r1 | `nls (split-usr) -static-libs`
sys-apps/attr-2.5.1-r2 | `nls (split-usr) -debug -static-libs`
sys-apps/debianutils-5.7 | `installkernel -static`
sys-apps/kmod-30-r1 | `lzma (tools) zlib zstd -debug -doc -pkcs7 -python -static-libs`
sys-apps/systemd-utils-252.9 | `acl kmod (split-usr) tmpfiles udev -boot (-selinux) -sysusers -test`
sys-apps/util-linux-2.38.1-r2 | `cramfs hardlink logger readline (split-usr) suid (unicode) -audit -build -caps -cryptsetup -fdformat -kill -magic -ncurses (-nls) -pam -python (-rtas) (-selinux) -slang -static-libs -su (-systemd) -test -tty-helpers -udev -verify-sig`
sys-fs/e2fsprogs-1.47.0-r2 | `(split-usr) tools -cron -fuse (-nls) -static-libs -test`
sys-fs/udev-init-scripts-35 | ``
sys-kernel/installkernel-gentoo-7 | `-grub`
sys-libs/libcap-2.69 | `(split-usr) -pam -static-libs -tools`
sys-libs/musl-1.2.3-r7 | `-crypt -headers-only -verify-sig`
sys-libs/ncurses-6.4_p20230401 | `cxx (split-usr) (tinfo) -ada -debug -doc -gpm -minimal -profile (-stack-realign) -static-libs -test -trace -verify-sig`
sys-libs/readline-8.1_p2-r1 | `(split-usr) (unicode) -static-libs -utils -verify-sig`
sys-libs/zlib-1.2.13-r1 | `(split-usr) -minizip -static-libs -verify-sig`
#### Inherited
Package | USE Flags
--------|----------
**FROM kubler/busybox** |
sys-apps/busybox-1.34.1-r1 | `make-symlinks static -debug -ipv6 -livecd -math -mdev -pam -savedconfig (-selinux) -sep-usr -syslog (-systemd)`

#### Purged
- [x] Headers
- [x] Static Libs
