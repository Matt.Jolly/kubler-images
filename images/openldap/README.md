## kangie/openldap

Run this [openldap][] image with:

    $ docker run -d --name openldap kangie/openldap

[Last Build][packages]

[openldap]: https://www.openldap.org/
[packages]: PACKAGES.md
