#!/usr/bin/env sh

set -e

# Super basic 'did the package compile remotely correctly' sanity check
# this may end up in lib64 depending on base image / profile shenanigans
/usr/lib/openldap/lloadd -VV  2>&1 | grep -i "OpenLDAP" || die

exit 0
