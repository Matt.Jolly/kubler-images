## kangie/elog

Run this [elog][] image with:

    $ docker run -d --name elog kangie/elog

This would be lighter without bash and imagemagick, just saying

[Last Build][packages]

[elog]: https://elog.psi.ch/elog
[packages]: PACKAGES.md
