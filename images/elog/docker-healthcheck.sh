#!/usr/bin/env sh

set -e

# Can we connect to a running elogd instance?
curl --fail http://localhost:8080 || exit 1

exit 0
