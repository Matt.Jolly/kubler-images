#
# Kubler phase 1 config, pick installed packages and/or customize the build
#

# The recommended way to install software is setting ${_packages}
_packages="
    net-misc/curl
    media-gfx/imagemagick
    www-apps/elog
"
# Install a standard system directory layout at ${_EMERGE_ROOT}, optional, default: false
#BOB_INSTALL_BASELAYOUT=true

# Remove specified binary cache file(s) *once* for given tag key..
#_no_cache_20200731="sys-apps/bash foo/bar/bar-0.1.2-r3.xpak"
# ..or omit the tag to always remove given binary cache file(s).
#_no_cache="sys-apps/bash"

# Define custom variables to your liking
#_elog_version=1.0

#
# This hook can be used to configure the build container itself, install packages, run any command, etc
#
configure_builder()
{
    # The package we want to install isn't in ::gentoo
    add_overlay kangie https://github.com/kangie/kangie-tools.git
    # rust-bin is waaaay faster and doesn't have a bdep on a fully-featured curl
    mask_package dev-lang/rust
    # we build a minimal curl for health checks
    unprovide_package net-misc/curl
    # https://bugs.gentoo.org/910526
    emerge -v1 dev-libs/glib
    :
}

#
# This hook is called just before starting the build of the root fs
#
configure_rootfs_build()
{
    # elog is only keyworded as testing
    update_keywords 'www-apps/elog' '+~amd64'
    # Make sure we don't build with pam
    update_use 'www-apps/elog' '-pam'

    # Let's grab the shiniest version of cURL and build a minimal configuration
    update_keywords 'net-misc/curl' '+~amd64'
    update_use 'net-misc/curl' '-use::*' '+ssl' '+openssl' '+curl_ssl_openssl'

    update_keywords 'media-gfx/imagemagick' '+~amd64'
    update_use 'media-gfx/imagemagick' '-use::*' '+jpeg' '+png' '+svg' '+xml' '+webp'

    # pulls in gcc||clang[openmp] as a runtime dependency
    update_use 'app-crypt/libb2' '-openmp'
    # no python runtime please!
    update_use 'dev-libs/libxml2' '-python'
    update_use 'gnome-base/librsvg' '-introspection' '-vala'
    update_use 'media-libs/harfbuzz' '-introspection'
    update_use 'x11-libs/gdk-pixbuf' '-introspection'
    update_use 'x11-libs/pango' '-introspection'
    # standard su issues for lightweight containers/chroots
    update_use 'sys-apps/util-linux' '-su'
    :
}

#
# This hook is called just before packaging the root fs tar ball, ideal for any post-install tasks, clean up, etc
#
finish_rootfs_build()
{
    cp -p /emerge-root/etc/elog/elogd.cfg.example /emerge-root/etc/elog/elogd.cfg || die failed to copy elogd default configuration!
    # Not required with imagemagick installed, but for a lightweight image elog will fail so this is safest
    copy_gcc_libs
    :
}
