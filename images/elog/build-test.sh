#!/usr/bin/env sh

# Note: pipefail is not supported in POSIX shell and will be silently ignored, unless bash is used
set -e

# basic sanity check; does the elog binary output version info when it's invoked?
/usr/bin/elog --help | head -n 1 | grep -q elogd || exit 1

exit 0
